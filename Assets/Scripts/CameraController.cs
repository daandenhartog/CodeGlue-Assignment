﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    
    public float horizontalSpeed = 3f;

    private List<Transform> targets = new List<Transform>();

    private void Awake() {
        foreach (Player player in FindObjectsOfType<Player>())
            targets.Add(player.transform);
    }

    private void Update() {
        if (targets == null || targets.Count == 0)
            return;

        Vector2 position = Vector2.Lerp(transform.position, GetAverageTargetPosition(), horizontalSpeed * Time.deltaTime);
        transform.position = new Vector3(position.x, position.y, transform.position.z);
    }

    private Vector2 GetAverageTargetPosition() {
        Vector2 origin = Vector2.zero;
        foreach (Transform target in targets)
            origin += (Vector2)target.position;

        return origin / targets.Count;
    }
}