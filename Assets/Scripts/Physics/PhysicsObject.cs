﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour {

    public BoxCollider2D boxCollider;
    public int horizontalRayCount = 3;
    public int verticalRayCount = 3;

    protected RaycastOrigins raycastOrigins;

    protected float horizontalRaySpacing;
    protected float verticalRaySpacing;

    protected const float skinWidth = 0.05f;

    public virtual void Initialize() {
        Bounds bounds = GetBoxColliderBoundsWithSkinWidth();

        horizontalRaySpacing = GetRaySpacing(bounds.size.x, horizontalRayCount);
        verticalRaySpacing = GetRaySpacing(bounds.size.y, verticalRayCount);
    }

    protected void SetRaycastOrigins() {
        Bounds bounds = GetBoxColliderBoundsWithSkinWidth();
        
        raycastOrigins.leftBottom = new Vector2(bounds.min.x, bounds.min.y);
        raycastOrigins.rightBottom = new Vector2(bounds.max.x, bounds.min.y);
        raycastOrigins.leftTop = new Vector2(bounds.min.x, bounds.max.y);
        raycastOrigins.rightTop = new Vector2(bounds.max.x, bounds.max.y);
    }

    private float GetRaySpacing(float width, int rayCount) {
        return width / (rayCount - 1);
    }

    private Bounds GetBoxColliderBoundsWithSkinWidth() {
        Bounds bounds = boxCollider.bounds;
        bounds.Expand(skinWidth * -2);
        return bounds;
    }

    protected struct RaycastOrigins {
        public Vector2 leftBottom, rightBottom;
        public Vector2 leftTop, rightTop;
    }
}