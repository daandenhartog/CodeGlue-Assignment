﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableObject : PhysicsObject {

    [HideInInspector] public LayerMask layerCollision = 1 << 0;
    [HideInInspector] public LayerMask layerMovableObject = 1 << 8;

    public Collisions collisions;

    [HideInInspector] public MoveableObject carriedObject;
    [HideInInspector] public float mass = 1f;
    [HideInInspector] public bool gravityInverted = false;

    public Vector2 Move(Vector2 velocity) {
        SetRaycastOrigins();
        collisions.Reset();

        // Carried object is reset because no collision with this object is found
        if (carriedObject != null && ((!carriedObject.collisions.bottom && !gravityInverted) || (!carriedObject.collisions.top && gravityInverted)))
            carriedObject = null;

        if(velocity.x != 0f) {
            velocity.x = GetHorizontalMovement(velocity.x);
        }

        if(velocity.y != 0f) {
            velocity.y = GetVerticalMovement(velocity.y);
        }

        // Update position and move carried object if applicable
        transform.position += (Vector3)velocity;
        if (carriedObject != null) {
            carriedObject.Move(velocity);

            if (!gravityInverted) { carriedObject.collisions.bottom = true; } 
            else { carriedObject.collisions.top = true; }
        }

        return velocity;
    }

    private float GetObjectPushedDistance(MoveableObject moveableObject, float pushDistance, int direction) {
        // Move moveableObject by pushDistance taking mass into account
        float pushModifier = mass / (mass + moveableObject.mass);
        return Mathf.Abs(moveableObject.Move(Vector2.right * pushDistance * pushModifier * direction).x);
    }

    private float GetHorizontalMovement(float velocity) {
        int direction = velocity < 0 ? -1 : 1;
        RaycastHit2D hit = GetClosestRaycastsHit(verticalRayCount, verticalRaySpacing, Mathf.Abs(velocity), 
            direction < 0 ? raycastOrigins.leftBottom : raycastOrigins.rightBottom, 
            direction < 0 ? Vector2.left : Vector2.right, 
            Vector2.up, layerCollision | layerMovableObject);

        if (!hit) {
            return velocity;
        } else {
            float distance = Mathf.Abs(hit.distance) - skinWidth;

            // Try and push other moveableObject
            if (1 << hit.collider.gameObject.layer == layerMovableObject) {
                float pushDistance = Mathf.Abs(velocity) - distance;
                float pushedDistance = GetObjectPushedDistance(hit.collider.GetComponent<MoveableObject>(), pushDistance, direction);
                distance += pushedDistance;
            }

            collisions.left = direction < 0;
            collisions.right = direction > 0;

            return distance * direction;
        }
    }

    private float GetVerticalMovement(float velocity) {
        int direction = velocity < 0 ? -1 : 1;
        RaycastHit2D hit = GetClosestRaycastsHit(horizontalRayCount, horizontalRaySpacing, Mathf.Abs(velocity), 
            direction < 0 ? raycastOrigins.leftBottom : raycastOrigins.leftTop, 
            direction < 0 ? Vector2.down : Vector2.up, 
            Vector2.right, layerCollision | layerMovableObject);

        if (!hit) {
            return velocity;
        } else {
            float distance = Mathf.Abs(hit.distance) - skinWidth;

            // If another MoveableObject is touched while in the air
            if (1 << hit.collider.gameObject.layer == layerMovableObject) {

                MoveableObject other = hit.collider.GetComponent<MoveableObject>();
                
                if ((direction < 0 && !gravityInverted) || (direction > 0 && gravityInverted)) {                    
                    // Attach ourselves to the other movable object if we're falling down
                    if (other.carriedObject == null) {
                        other.carriedObject = this;
                    }
                } else {
                    // Attach the other movable object to us and continue the jump if possible
                    if (carriedObject == null) {
                        carriedObject = other;
                    }

                    return velocity;
                }
            }

            collisions.bottom = direction < 0;
            collisions.top = direction > 0;

            return distance * direction;
        }
    }

    private RaycastHit2D GetClosestRaycastsHit(int rayCount, float raySpacing, float rayLength, Vector2 origin, Vector2 rayDirection, Vector2 spacingDirection, LayerMask layerMask) {
        RaycastHit2D hit = new RaycastHit2D();

        // Send out several raycasts and return the closests found hit
        for(int i = 0; i < rayCount; i++) {
            Vector2 rayOrigin = origin + spacingDirection * (raySpacing * i);
            RaycastHit2D rayHit = Physics2D.Raycast(rayOrigin, rayDirection, rayLength + skinWidth, layerMask);

            if(rayHit && (!hit || (rayHit.distance < hit.distance))) {
                hit = rayHit;
            }
        }

        return hit;
    }

    public struct Collisions { 

        public bool left, right;
        public bool bottom, top;

        public void Reset() {
            left = right = false;
            bottom = top = false;
        }
    }
}