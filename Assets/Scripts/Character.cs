﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Character")]
public class Character : ScriptableObject {

    public GameObject model;
    public RuntimeAnimatorController animatorController;

    public KeyCode moveLeft, moveRight, moveUp;

    public Vector2 boxColliderSize = new Vector2(0.4f, 0.8f);
    public float movementSpeed = 4f;
    public float jumpHeight = 5f;
    public float gravity = -20f;
    public float mass = 1f;
}
