﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour {
        
    public Transform modelRotation;
    public Transform modelPivot;

    private enum Facing { LEFT, RIGHT };
    private Facing facing = Facing.RIGHT;

    private Animator animator;
    private bool gravityInverted;
    
    public void Initialize(Character character) {
        Transform model = GetInstantiatedModel(character.model);

        animator = model.GetComponent<Animator>();
        animator.applyRootMotion = false;
        animator.runtimeAnimatorController = character.animatorController;

        modelPivot.localPosition = Vector2.down * character.boxColliderSize.y / 2f;

        transform.name = string.Format("Player_{0}", character.name);
    }

    public void SetAnimations(float velocityX, float velocityY) {
        animator.SetBool("velocityX", !(velocityX > -0.05f && velocityX < 0.05f));
        animator.SetBool("velocityY", velocityY != 0f);
    }

    public void SetJumping() {
        PlayAnimation("Jump");
    }

    public void SetFacing(float velocityX) {
        if((velocityX < 0 && facing != Facing.LEFT) || velocityX > 0 && facing != Facing.RIGHT) {
            facing = velocityX < 0 ? Facing.LEFT : Facing.RIGHT;
            SetRotation();
        }
    }

    public void SetGravityInversion(bool gravityInverted) {
        this.gravityInverted = gravityInverted;
        SetRotation();
    }

    private void SetRotation() {
        modelRotation.eulerAngles = new Vector3((!gravityInverted ? 0 : 180), (facing == Facing.LEFT ? 180 : 0), 0);
    }

    private void PlayAnimation(string anim) {
        animator.Play(anim);
    }

    private Transform GetInstantiatedModel(GameObject prefab) {
        Transform model = Instantiate(prefab).transform;
        model.SetParent(modelPivot);
        model.localPosition = Vector2.zero;
        model.localEulerAngles = Vector3.up * 90f;
        model.name = prefab.name;

        return model;
    }
}