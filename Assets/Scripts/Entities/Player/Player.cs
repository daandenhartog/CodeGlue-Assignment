﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Character character;

    private PlayerView view;
    private MoveableObject moveableObject;

    private Vector2 startingPosition;
    private Vector2 velocity;

    private float movementSpeed;
    private float jumpHeight;
    private float gravity;

    private const float requiredPortalVelocity = 3.4f;
    
    private void Awake() {
        view = GetComponent<PlayerView>();
        view.Initialize(character);

        moveableObject = GetComponent<MoveableObject>();
        moveableObject.boxCollider.size = character.boxColliderSize;
        moveableObject.mass = character.mass;
        moveableObject.Initialize();

        movementSpeed = character.movementSpeed;
        jumpHeight = character.jumpHeight;
        gravity = character.gravity;

        startingPosition = transform.position;
    }

    public void OnTouchCrawler() {
        ResetSelf();
    }

    private void Update() {
        velocity.x = GetHorizontalInput().x * movementSpeed;
        velocity.y += gravity * Time.deltaTime;

        if (GetJumps()) {
            velocity.y = jumpHeight;
            view.SetJumping();
        }

        Vector2 movedDistance = moveableObject.Move(velocity * Time.deltaTime);
        
        DetectGravityInversion();

        if (IsGrounded())
            velocity.y = 0.0f;

        view.SetFacing(velocity.x);
        view.SetAnimations(movedDistance.x, velocity.y);
    }

    private void ResetSelf() {
        velocity = Vector2.zero;
        transform.position = startingPosition;
    }

    private void DetectGravityInversion() {
        if ((transform.position.y <= 0f && !moveableObject.gravityInverted) || (transform.position.y >= 0f && moveableObject.gravityInverted)) {
            // Inverse gravity and jumpheight
            moveableObject.gravityInverted = !moveableObject.gravityInverted;
            gravity *= -1;
            jumpHeight *= -1;

            // Give a velocity boost so players can always get out of the portal
            if (Mathf.Abs(velocity.y) < requiredPortalVelocity) {
                velocity.y = requiredPortalVelocity * Mathf.Sign(velocity.y);
            }
            
            view.SetGravityInversion(moveableObject.gravityInverted);
        }
    }

    private Vector2 GetHorizontalInput() {
        Vector2 input = Vector2.zero;
        if (Input.GetKey(character.moveLeft)) input += Vector2.left;
        if (Input.GetKey(character.moveRight)) input += Vector2.right;
        return input;
    }

    private bool GetJumps() {
        return Input.GetKeyDown(character.moveUp) && IsGrounded();
    }

    private bool IsGrounded() {
        return (moveableObject.collisions.bottom && !moveableObject.gravityInverted) || (moveableObject.collisions.top && moveableObject.gravityInverted);
    }
}