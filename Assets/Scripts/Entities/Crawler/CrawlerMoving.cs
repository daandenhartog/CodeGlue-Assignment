﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlerMoving : Crawler {

    public List<Transform> waypoints = new List<Transform>();
    public float movementSpeed = 3f;

    private int waypointIndex;

    private void Update() {
        if (waypoints.Count < 2)
            return;

        MoveTowardsWaypointHorizontal(waypoints[waypointIndex]);
    }

    private void MoveTowardsWaypointHorizontal(Transform waypoint) {
        int direction = waypoint.position.x < transform.position.x ? -1 : 1;

        float movingDistance = movementSpeed * Time.deltaTime;
        float distanceToWaypoint = Mathf.Abs(waypoint.position.x - transform.position.x);

        // Don't exceed target
        // Since waypoint is reached the next way point is set up
        if (distanceToWaypoint < movingDistance) {
            movingDistance = distanceToWaypoint;

            waypointIndex++;
            if (waypointIndex >= waypoints.Count) {
                waypointIndex = 0;
            }
        }

        transform.position += Vector3.right * movingDistance * direction;
    }
}