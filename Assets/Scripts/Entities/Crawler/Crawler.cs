﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Crawler : MonoBehaviour {

    protected virtual void Awake() {
        // Make sure RigidBody is set to kinematic
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
    }

    private void OnTriggerEnter2D(Collider2D col) {
        Player player = col.GetComponent<Player>();
        if (player == null)
            return;

        // Either reset Player or destroy self
        if (col.transform.position.y > transform.position.y) {
            player.OnTouchCrawler();
        } else {
            DestroySelf();
        }
    }

    private void DestroySelf() {
        Destroy(gameObject);
    }
}